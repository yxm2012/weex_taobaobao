import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/index'
import Login from '@/views/user/login'
import Register from '@/views/user/register'
import Forget from '@/views/user/forget'
import Guide from '@/views/user/guide'

import Teach from '@/views/news/teach'
import System from '@/views/news/system'

import Hello from '@/components/HelloWorld'
Vue.use(Router)

module.exports = new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },{
      path: '/login',
      name: 'Login',
      component: Login
    },{
      path: '/forget',
      name: 'Forget',
      component: Forget
    },{
      path: '/register',
      name: 'Register',
      component: Register
    },{
      path: '/guide',
      name: 'Guide',
      component: Guide
    },{
      path: '/teach',
      name: 'Teach',
      component: Teach
    },{
      path: '/system',
      name: 'System',
      component: System
    }
  ]
})
