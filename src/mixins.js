//import Vue from 'vue'
import weexVueRouter from 'weex-vue-router'
import routes from './native-router'//web端的路由在web-router里定义，在app.js里引用
Vue.use(weexVueRouter,{routes,weex});
export default {
    data() {
        return {
            rpx: 1,
            apiDomain:'',
            android:weex.config.env.platform.toLowerCase()=='android',
            ios:weex.config.env.platform.toLowerCase()=='ios',
            web:weex.config.env.platform.toLowerCase()=='web'
        }
    },
    created() {
        let self=this;
        let env = weex.config.env;
        let rWidth=env.deviceWidth;
        env.deviceWidth > 828 && (rWidth = env.deviceWidth / 3 * 2);
        self.rpx = 750 / rWidth;
        //self.apiDomain='http://xiazhou.me/example/xiazhou-weex';
        if(!self.web){
            self.apiDomain='http://192.168.5.40:8088';//替换成你电脑的IP，并保证手机能访问到电脑(连同一个wifi就好啦)
        }
    },
    methods: {
        jump(url) {
            /*链接有三种情况：
             * /product/20408.html  开发者定义跳转
             * http://xiazhou.me/#/product/20408.html?from=banner  运维添加的链接
             * http://xiazhou.me/blog/670.html?from=banner#tabs1   原生页面，一般native端都会做链接截取跳转对应页面
             * */
            if (!url || (url.indexOf('http') != 0 && url.indexOf('/') != 0)) {
                console.warn(url + "为非法的链接");
                return;
            }
            const self = this;
            //const nativeEvent = weex.requireModule('nativeEvent');//native端必须扩展这个模块才能正常运行
            let go = to => {
                self.$router.push(to);
            };
            url.indexOf('/') == 0 && go(url); //开发者定义跳转
            url.indexOf('http') == 0 && url.indexOf('/#/') > 0 && go(url.substr(url.indexOf('#') + 1)); //运维添加的链接
            //url.indexOf('http') == 0 && url.indexOf('/#/') == -1 && nativeEvent.skip(url); //原生页面//native端必须扩展这个模块才能正常运行
        }
    }
}
