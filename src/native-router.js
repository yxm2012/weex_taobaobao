//native-router.js
const domain='http://192.168.5.40:8088/dist';
//js bundle address，must end with '.js'
const routes = [{
    path:'/',
    component:domain+'/index.js'
},{
    path:'/login',
    component:domain+'/views/user/login.js'
},{
    path:'/forget',
    component:domain+'/views/user/forget.js'
},{
    path:'/register',
    component:domain+'/views/user/register.js'
},{
    path:'/guide',
    component:domain+'/views/user/guide.js'
},{
    path:'/teach',
    component:domain+'/views/news/teach.js'
},{
    path:'/system',
    component:domain+'/views/news/system.js'
}];
export default routes;