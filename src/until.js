export default{
    //定义插件的公共函数
    install(Vue){
        Vue.prototype.modal = weex.requireModule('modal');
        Vue.prototype.navigator = weex.requireModule('navigator');
        Vue.prototype.storage = weex.requireModule('storage');
        Vue.prototype.stream = weex.requireModule('stream');
        Vue.prototype.Authorization = '';
        Vue.prototype.baseURL = 'http://192.168.5.184:8019/';
        Vue.prototype.storage.getItem('token', event => {
            //this.token = event.data;
            if(event.data){
                //登录成功后，在加头信息，不认取的有误
                this.Authorization = event.data;
            }
        })
        Vue.config.productionTip = false;
        //公共get请求函数
        Vue.prototype.request = function (obj){
            return new Promise((resolve, reject)=>{
                this.stream.fetch({
                    method: obj.method,
                    url: this.baseURL+obj.url,
                    type:'json',
                    headers: {
                        'Content-Type':'application/x-www-form-urlencoded',
                        'Authorization':this.Authorization
                    },
                    body: this.toParams(obj.data)
                }, (res)=>{
                    this.confirm(res.data.info);
                    if(!res.data.code){
                        resolve(res.data);
                    }
                },(response)=>{
                    console.log(response);
                });
            });
        };
        // 工具方法
        Vue.prototype.toParams = function(obj) {
          var param = ""
          for(const name in obj) {
            if(typeof obj[name] != 'function') {
              param += "&" + name + "=" + encodeURI(obj[name])
            }
          }
          return param.substring(1)
        };

        //公共处理请求错误信
        Vue.prototype.errMessage = function(errorInfo){
            //store.state.comfirmMessage = errorInfo;
            if(errorInfo == "没有登录,请登录!"){
                //直接打开去登录弹框
                setTimeout(()=>{
                    this.$router.push('/login')
                },3000);
            }else{
                //弹出错误信息弹框
                this.confirm(errorInfo);
            }
        };
        Vue.prototype.jump = function(url){
            //this.$router.push(url)
        }
        Vue.prototype.confirm = function(message){
            this.modal.toast({ 'message': message, 'duration': 2 });
        }    
    }
}
